import {Logger} from "@nestjs/common";
import {OnGatewayConnection, OnGatewayDisconnect, OnGatewayInit, WebSocketGateway, WebSocketServer} from "@nestjs/websockets";
import {Server} from "socket.io";

@WebSocketGateway()
export class WebsocketService implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect{
    @WebSocketServer()
    server: Server;
    private logger: Logger = new Logger('WebSocketGateway');

    afterInit(server: any): any {
        this.logger.log('Gateway start');
    }

    handleConnection(client: any, ...args: any[]): any {
        this.logger.log(`Client connected`);
    }

    handleDisconnect(client: any): any {
        this.logger.log(`Client disconnected`);
    }

    // emitToAll(topic: string, content: string): void {
    //     this.server.emit(topic, content);
    // }

    initializeTopic() {
        this.server.on('offer', (event: Map<string, any>) => {
            this.server.emit('onOffer', event);
        });

        this.server.on('answer', (event: Map<string, any>) => {
            this.server.emit('onAnswer', event);
        });

        this.server.on('candidate', (event: Map<string, any>) => {
            this.server.emit('onCandidate', event);
        });
    }
}
